<?php

# load the library
require_once '../lib/load.php';

# display the party's event feed or the splash menu
if (party_selected()) {
    $content_view = 'index/feed-page.php';
    require_once VIEW_DIR . 'main-layout.php';
} else {
    $content_view = 'index/splash-page.php';
    require_once VIEW_DIR . 'splash-layout.php';
}
