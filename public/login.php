<?php

# load the library
require_once '../lib/load.php';

# the user may not already be logged in
require_user_not_logged_in();

# gather the input
$input = request_data(['email', 'password']);

# process the request
$errors = [];
if (request_method() == 'POST') {
    # validate the input
    if (!$input['email']) $errors[] = 'Email address is required.';
    if (!$input['password']) $errors[] = 'Password is required.';
    if (!$errors) {
        # try to find the user and verify the password
        $user = find_user_by_email($input['email']);
        if (!$user || !password_verify($input['password'], $user->password)) {
            $errors[] = 'Email or password was incorrect.';
        } else {
            # create the session
            $session_id = create_session([
                'user_id' => $user->id,
                'ip_address' => ip_address(),
                'user_agent' => user_agent()
            ]);
            set_session_cookie($session_id);
            # redirect to the party selection page
            redirect('parties.php');
        }
    }
}

# render the layout
$content_view = 'login/login-page.php';
require_once VIEW_DIR . 'splash-layout.php';
