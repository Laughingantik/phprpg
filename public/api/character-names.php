<?php

# load the library
require_once '../../lib/load.php';

# gather the input
$race = request_query('race');
if ($race) $race = character_race($race);
else $race = random_character_race();
$gender = request_query('gender');
if ($gender) $gender = character_gender($gender);
else $gender = random_character_gender();
$count = min(100, request_query('count', 1));

# todo: should return an error if invalid race, gender or count

# generate random character names
$names = [];
if ($race and $gender) {
    for ($i = 0; $i < $count; $i++) {
        $names[] = $race->generateRandomName($gender);
    }
}

# render the names
render_json([
    'race' => $race ? $race->name : null,
    'gender' => $gender ? $gender->name : null,
    'names' => $names
]);
