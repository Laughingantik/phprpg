<?php

# load the library
require_once '../lib/load.php';

# the user may not already be logged in
require_user_not_logged_in();

# gather the input
$input = request_data(['email', 'password', 'password_confirmation']);

# process the request
$errors = [];
if (request_method() == 'POST') {
    # validate the input
    if (!$input['email']) $errors[] = 'Email address is required.';
    else if (find_user_by_email($input['email']))
        $errors[] = 'Email address is already in use.';
    if (!$input['password']) $errors[] = 'Password is required.';
    if (!$input['password_confirmation'])
        $errors[] = 'Password confirmation is required.';
    else if ($input['password'] != $input['password_confirmation'])
        $errors[] = 'Password and password confirmation do not match.';
    if (!$errors) {
        # hash the password using bcrypt
        $password = password_hash($input['password'], PASSWORD_BCRYPT);
        # create the user
        create_user(['email' => $input['email'], 'password' => $password]);
        # redirect to the login page
        # todo: should show a success message to the user and prompt them to
        #     go to the login page and log in instead
        redirect('login.php');
    }
}

# render the layout
$content_view = 'register/register-page.php';
require_once VIEW_DIR . 'splash-layout.php';
