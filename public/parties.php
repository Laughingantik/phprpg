<?php

# load the library
require_once '../lib/load.php';

# the user must be logged in
require_user_logged_in();

# process the request
$errors = [];
if (request_action() == 'create') {
    $content_view = 'parties/create-party-form.php';
    # gather the input
    $input = request_data(['name', 'race', 'gender']);
    if (request_method() == 'POST') {
        # validate the input
        if (!$input['name']) $errors[] = 'Name is required.';
        if (!$input['race']) $errors[] = 'Race is required.';
        else if (!character_race($input['race']))
            $errors[] = 'Race is an invalid race.';
        if (!$input['gender']) $errors[] = 'Gender is required.';
        else if (!character_gender($input['gender']))
            $errors[] = 'Gender is an invalid gender.';
        if (!$errors) {
            # create the party leader and party
            $character_id = create_character([
                'name' => $input['name'],
                'race' => $input['race'],
                'gender' => $input['gender']
            ]);
            $party_id = create_party([
                'user_id' => current_session()->user_id,
                'member_1_id' => $character_id
            ]);
            # select the new party
            redirect("parties.php?select=$party_id");
        }
    }
} else if (request_action() == 'select') {
    $party_id = request_query('id');
    $party = find_party_by_id($party_id);
    $session = current_session();
    # make sure the user owns the party
    if ($party->user_id == $session->user_id) {
        set_session_party($session, $party);
        redirect('index.php');
    } else {
        redirect('parties.php');
    }
} else {
    $content_view = 'parties/party-list.php';
}

# render the layout
require_once VIEW_DIR . 'splash-layout.php';
