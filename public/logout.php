<?php

# load the library
require_once '../lib/load.php';

# the user must be logged in
require_user_logged_in();

# revoke the user's session and redirect to the login page
revoke_session(current_session());
unset_session_cookie();
redirect('login.php');
