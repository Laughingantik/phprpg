<?php


function set_session_cookie($session_id) {
    setcookie('session_id', $session_id);
}


function unset_session_cookie() {
    setcookie('session_id', '', time() - 3600);
}


function current_session() {
    static $session = null;
    if (!$session and isset($_COOKIE['session_id'])) {
        $session = find_session_by_id($_COOKIE['session_id']);
        if (session_expired($session)) {
            revoke_session($session);
            unset_session_cookie();
            $session = null;
            redirect('login.php');
        } else if ($session->revoked) {
            unset_session_cookie();
            $session = null;
            redirect('login.php');
        } else {
            refresh_session($session);
        }
    }
    return $session;
}


function require_user_logged_in() {
    if (!user_logged_in()) redirect('login.php');
}


function require_user_not_logged_in() {
    if (user_logged_in()) redirect('index.php');
}


function user_logged_in() {
    return current_user() != null;
}


function current_user() {
    if (!current_session()) return null;
    return current_session()->user;
}


function require_party_selected() {
    require_user_logged_in();
    if (!party_selected()) redirect('parties.php');
}


function party_selected() {
    return current_party() != null;
}


function current_party() {
    if (!current_session()) return null;
    return current_session()->party;
}


function current_party_members() {
    if (!current_party()) return [];
    return current_party()->members;
}


function ip_address() {
    return isset($_SERVER['REMOTE_ADDR']) ? $_SERVER['REMOTE_ADDR'] : null;
}


function user_agent() {
    return isset($_SERVER['HTTP_USER_AGENT']) ?
        $_SERVER['HTTP_USER_AGENT'] : null;
}
