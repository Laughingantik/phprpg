<?php

function db_connect() {
    static $handler = null;
    if ($handler == null) {
        $dsn = 'mysql:host=' . DB_HOST . ';dbname=' . DB_NAME;
        $handler = new PDO($dsn, DB_USERNAME, DB_PASSWORD);
    }
    return $handler;
}


function db_query_first($sql, $params = null) {
    return db_query($sql, $params)->fetch(PDO::FETCH_ASSOC);
}


function db_query_all($sql, $params = null) {
    return db_query($sql, $params)->fetchAll(PDO::FETCH_ASSOC);
}


function db_query($sql, $params = null) {
    $handler = db_connect();
    $statement = null;
    if ($params) {
        $statement = $handler->prepare($sql);
        foreach ($params as $key => &$value) {
            if (is_int($key)) $key = $key + 1;
            $statement->bindParam($key, $value);
        }
    } else {
        $statement = $handler->query($sql);
    }
    $statement->execute();
    return $statement;
}


function db_create_id() {
    $result = db_query_first('SELECT UUID_SHORT() AS uuid');
    return (int) $result['uuid'];
}
