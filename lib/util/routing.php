<?php


function request_query($key, $default = null) {
    return isset($_GET[$key]) ? $_GET[$key] : $default;
}


function request_data($key, $default = null) {
    if (is_array($key))
        return array_combine($key, array_map('request_data', $key));
    return isset($_POST[$key]) ? $_POST[$key] : $default;
}


function request_method() {
    return isset($_SERVER['REQUEST_METHOD']) ?
        $_SERVER['REQUEST_METHOD'] : null;
}


function request_action() {
    return request_query('action');
}


function redirect($location, $immediate = true) {
    header("Location: $location");
    if ($immediate) exit();
}


function render_json($value) {
    header('Content-Type: application/json');
    echo json_encode($value);
}
