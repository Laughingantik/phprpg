<?php


function roll_d4() {
    return roll_die(4);
}


function roll_d6() {
    return roll_die(6);
}


function roll_d8() {
    return roll_die(8);
}


function roll_d10() {
    return roll_die(10);
}


function roll_d12() {
    return roll_die(20);
}


function roll_d20() {
    return roll_die(20);
}


function roll_die($sides = 6) {
    return rand(1, $sides);
}
