<?php

# define directory paths
define('LIB_DIR', __DIR__ . '/');

# define library paths
define('MODEL_DIR', LIB_DIR . 'models/');
define('UTIL_DIR', LIB_DIR . 'util/');
define('VIEW_DIR', LIB_DIR . 'views/');

# define environment
define('ENV', getenv('ENV'));

# define database connection info
define('DB_HOST', getenv('DB_HOST') ?: 'localhost');
define('DB_NAME', getenv('DB_NAME') ?: 'phprpg');
define('DB_USERNAME', getenv('DB_USERNAME') ?: 'phprpg');
define('DB_PASSWORD', getenv('DB_PASSWORD') ?: 'password');

# define session settings
define('SESSION_TIMEOUT', 1800);

# configure error handling
error_reporting(E_ALL);
if (ENV == 'production') {
    ini_set('display_errors', 0);
    ini_set('display_startup_errors', 0);
} else {
    ini_set('display_errors', 1);
    ini_set('display_startup_errors', 1);
}

# import utilities
require_once UTIL_DIR . 'authentication.php';
require_once UTIL_DIR . 'database.php';
require_once UTIL_DIR . 'dice.php';
require_once UTIL_DIR . 'routing.php';

# import models
require_once MODEL_DIR . 'character.php';
require_once MODEL_DIR . 'location.php';
require_once MODEL_DIR . 'party.php';
require_once MODEL_DIR . 'session.php';
require_once MODEL_DIR . 'user.php';

# import character races
require_once MODEL_DIR . 'character-races/dwarf.php';
require_once MODEL_DIR . 'character-races/elf.php';
require_once MODEL_DIR . 'character-races/human.php';

# import locations
require_once MODEL_DIR . 'locations/gariki.php';
require_once MODEL_DIR . 'locations/the_abysmal_burrows.php';
