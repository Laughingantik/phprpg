<?php


class Party {

    public $id = null;
    public $user_id = null;
    public $name = '(Unknown)';
    public $member_1_id = null;
    public $member_2_id = null;
    public $member_3_id = null;
    public $member_4_id = null;
    public $member_5_id = null;
    public $member_6_id = null;
    public $gold_coins = 0;
    public $silver_coins = 0;
    public $copper_coins = 0;
    public $created_at = null;
    public $updated_at = null;
    public $cached_user = null;
    public $cached_members = null;

    static function fromArray($data) {
        if ($data == null) return null;
        $party = new Party();
        if (isset($data['id'])) $party->id = $data['id'];
        $party->user_id = $data['user_id'];
        if (isset($data['name'])) $party->name = $data['name'];
        $party->member_1_id = $data['member_1_id'];
        if (isset($data['member_2_id']))
            $party->member_2_id = $data['member_2_id'];
        if (isset($data['member_3_id']))
            $party->member_3_id = $data['member_3_id'];
        if (isset($data['member_4_id']))
            $party->member_4_id = $data['member_4_id'];
        if (isset($data['member_5_id']))
            $party->member_5_id = $data['member_5_id'];
        if (isset($data['member_6_id']))
            $party->member_6_id = $data['member_6_id'];
        if (isset($data['gold_coins']))
            $party->gold_coins = $data['gold_coins'];
        if (isset($data['silver_coins']))
            $party->silver_coins = $data['silver_coins'];
        if (isset($data['copper_coins']))
            $party->copper_coins = $data['copper_coins'];
        if (isset($data['created_at']))
            $party->created_at = $data['created_at'];
        if (isset($data['updated_at']))
            $party->updated_at = $data['updated_at'];
        return $party;
    }

    static function fromArrays($data) {
        return array_map('Party::fromArray', $data);
    }

    public function __get($name) {
        switch ($name) {
            case 'user': return $this->getUser();
            case 'leader': return $this->member_1;
            case 'member_1': return $this->getMember(1);
            case 'member_2': return $this->getMember(2);
            case 'member_3': return $this->getMember(3);
            case 'member_4': return $this->getMember(4);
            case 'member_5': return $this->getMember(5);
            case 'member_6': return $this->getMember(6);
            case 'members': return $this->getMembers();
            default:
                throw new InvalidArgumentException("Invalid property: $name");
        }
    }

    private function getUser() {
        if (!$this->cached_user and $this->user_id)
            $this->cached_user = find_user_by_id($this->user_id);
        return $this->cached_user;
    }

    private function getMember($index) {
        if (!$this->cached_members) $this->cached_members = [];
        if (!isset($this->cached_members[$index])) {
            $id = $this->{"member_${index}_id"};
            if (!$id) $this->cached_members[$index] = null;
            else $this->cached_members[$index] = find_character_by_id($id);
        }
        return $this->cached_members[$index];
    }

    private function getMembers() {
        return array_filter([
            $this->member_1, $this->member_2, $this->member_3, $this->member_4,
            $this->member_5, $this->member_6
        ]);
    }

}


function create_party($party) {
    $id = db_create_id();
    db_query(
        '
            INSERT INTO parties (id, user_id, member_1_id)
            VALUES (:id, :user_id, :member_1_id)
        ',
        [
            ':id' => $id,
            ':user_id' => $party['user_id'],
            ':member_1_id' => $party['member_1_id']
        ]
    );
    return $id;
}


function find_party_by_id($id) {
    return Party::fromArray(db_query_first(
        'SELECT * FROM parties WHERE id = :id',
        [':id' => $id]
    ));
}


function find_parties_by_user_id($user_id) {
    return Party::fromArrays(db_query_all(
        'SELECT * FROM parties WHERE user_id = :user_id',
        [':user_id' => $user_id]
    ));
}
