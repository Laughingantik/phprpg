<?php


class CharacterRace {

    public $name = null;
    public $first_names = [];
    public $last_names = [];
    public $strength_modifier = 0;
    public $dexterity_modifier = 0;
    public $constitution_modifier = 0;
    public $intelligence_modifier = 0;
    public $wisdom_modifier = 0;
    public $charisma_modifier = 0;

    public function generateRandomName($gender = null) {
        if (!$gender) $gender = random_character_gender();
        # generate a random first name
        $first_names = $this->first_names[character_gender_key($gender)];
        if (!$first_names) return null;
        $first = $first_names[array_rand($first_names)];
        # generate a random last name
        $last_names = $this->last_names;
        if (!$last_names) return null;
        $last = $last_names[array_rand($last_names)];
        # return the full name
        return "$first $last";
    }

}


function random_character_race() {
    return character_races()[random_character_race_key()];
}


function random_character_race_key() {
    $keys = character_race_keys();
    return $keys[array_rand($keys)];
}


function character_race_keys() {
    return array_keys(character_races());
}


function character_race_key($race) {
    foreach (character_races() as $key => $value) {
        if ($race == $value) return $key;
    }
    return null;
}


function character_races() {
    return character_race();
}


function character_race($key = null, $value = null) {
    static $races = [];
    if ($key and $value) $races[$key] = $value;
    if (!$key) return $races;
    if (array_key_exists($key, $races)) return $races[$key];
    return null;
}


class CharacterGender {

    public $name = null;

}


function random_character_gender() {
    return character_genders()[random_character_gender_key()];
}


function random_character_gender_key() {
    $keys = character_gender_keys();
    return $keys[array_rand($keys)];
}


function character_gender_keys() {
    return array_keys(character_genders());
}


function character_gender_key($gender) {
    foreach (character_genders() as $key => $value) {
        if ($gender == $value) return $key;
    }
    return null;
}


function character_genders() {
    return character_gender();
}


function character_gender($key = null, $value = null) {
    static $genders = [];
    if ($key and $value) $genders[$key] = $value;
    if (!$key) return $genders;
    if (array_key_exists($key, $genders)) return $genders[$key];
    return null;
}


$male = character_gender('male', new CharacterGender());
$male->name = 'Male';

$female = character_gender('female', new CharacterGender());
$female->name = 'Female';

$nonbinary = character_gender('nonbinary', new CharacterGender());
$nonbinary->name = 'Non-binary';


class Character {

    public $id = null;
    public $name = null;
    public $race = null;
    public $gender = null;
    public $experience_total = 0;
    public $strength_base = 5;
    public $dexterity_base = 5;
    public $constitution_base = 5;
    public $intelligence_base = 5;
    public $wisdom_base = 5;
    public $charisma_base = 5;
    public $wounds = 0;
    public $created_at = null;
    public $updated_at = null;

    static function fromArray($data) {
        if ($data == null) return null;
        $character = new Character();
        if (isset($data['id'])) $character->id = $data['id'];
        if (isset($data['name'])) $character->name = $data['name'];
        $character->race = character_race($data['race']);
        $character->gender = character_gender($data['gender']);
        if (isset($data['experience_total']))
            $character->experience_total = $data['experience_total'];
        if (isset($data['strength_base']))
            $character->strength_base = $data['strength_base'];
        if (isset($data['dexterity_base']))
            $character->dexterity_base = $data['dexterity_base'];
        if (isset($data['constitution_base']))
            $character->constitution_base = $data['constitution_base'];
        if (isset($data['intelligence_base']))
            $character->intelligence_base = $data['intelligence_base'];
        if (isset($data['intelligence_base']))
            $character->intelligence_base = $data['intelligence_base'];
        if (isset($data['wisdom_base']))
            $character->wisdom_base = $data['wisdom_base'];
        if (isset($data['charisma_base']))
            $character->charisma_base = $data['charisma_base'];
        if (isset($data['wounds'])) $character->wounds = $data['wounds'];
        if (isset($data['created_at']))
            $character->created_at = $data['created_at'];
        if (isset($data['updated_at']))
            $character->updated_at = $data['updated_at'];
        return $character;
    }

    static function fromArrays($data) {
        return array_map('Character::fromArray', $data);
    }

    function __get($name) {
        switch ($name) {
            case 'level': return $this->getLevel();
            case 'level_next': return $this->getLevelNext();
            case 'experience': return $this->getExperience();
            case 'experience_next': return $this->getExperienceNext();
            case 'experience_total_current':
                return $this->getExperienceTotalCurrent();
            case 'experience_total_next':
                return $this->getExperienceTotalNext();
            case 'attribute_points_used':
                return $this->getAttributePointsUsed();
            case 'attribute_points_total':
                return $this->getAttributePointsTotal();
            case 'attribute_points': return $this->getAttributePoints();
            case 'strength': return $this->getStrength();
            case 'strength_modifier': return $this->getStrengthModifier();
            case 'strength_modifier_race':
                return $this->getStrengthModifierRace();
            case 'strength_modifier_equipment':
                return $this->getStrengthModifierEquipment();
            case 'dexterity': return $this->getDexterity();
            case 'dexterity_modifier': return $this->getDexterityModifier();
            case 'dexterity_modifier_race':
                return $this->getDexterityModifierRace();
            case 'dexterity_modifier_equipment':
                return $this->getDexterityModifierEquipment();
            case 'constitution': return $this->getConstitution();
            case 'constitution_modifier':
                return $this->getConstitutionModifier();
            case 'constitution_modifier_race':
                return $this->getConstitutionModifierRace();
            case 'constitution_modifier_equipment':
                return $this->getConstitutionModifierEquipment();
            case 'intelligence': return $this->getIntelligence();
            case 'intelligence_modifier':
                return $this->getIntelligenceModifier();
            case 'intelligence_modifier_race':
                return $this->getIntelligenceModifierRace();
            case 'intelligence_modifier_equipment':
                return $this->getIntelligenceModifierEquipment();
            case 'wisdom': return $this->getWisdom();
            case 'wisdom_modifier': return $this->getWisdomModifier();
            case 'wisdom_modifier_race': return $this->getWisdomModifierRace();
            case 'wisdom_modifier_equipment':
                return $this->getWisdomModifierEquipment();
            case 'charisma': return $this->getCharisma();
            case 'charisma_modifier': return $this->getCharismaModifier();
            case 'charisma_modifier_race':
                return $this->getCharismaModifierRace();
            case 'charisma_modifier_equipment':
                return $this->getCharismaModifierEquipment();
            case 'hit_points': return $this->getHitPoints();
            case 'hit_points_max': return $this->getHitPointsMax();
            default:
                throw new InvalidArgumentException("Invalid property: $name");
        }
    }

    private function getLevel() {
        return experience_to_level($this->experience_total);
    }

    private function getLevelNext() {
        return $this->level + 1;
    }

    private function getExperience() {
        return $this->experience_total - $this->experience_total_current;
    }

    private function getExperienceNext() {
        return $this->experience_total_next - $this->experience_total_current;
    }

    private function getExperienceTotalCurrent() {
        return level_to_experience($this->level);
    }

    private function getExperienceTotalNext() {
        return level_to_experience($this->level_next);
    }

    private function getAttributePointsUsed() {
        return $this->strength_base + $this->dexterity_base
            + $this->constitution_base + $this->intelligence_base
            + $this->wisdom_base + $this->charisma_base;
    }

    private function getAttributePointsTotal() {
        return 30 + $this->level * 6;
    }

    private function getAttributePoints() {
        return max(0, $this->attribute_points_total
            - $this->attribute_points_used);
    }

    private function getStrength() {
        return $this->strength_base + $this->strength_modifier;
    }

    private function getStrengthModifier() {
        return $this->strength_modifier_race
            + $this->strength_modifier_equipment;
    }

    private function getStrengthModifierRace() {
        return $this->level * $this->race->strength_modifier;
    }

    private function getStrengthModifierEquipment() {
        return 0;
    }

    private function getDexterity() {
        return $this->dexterity_base + $this->dexterity_modifier;
    }

    private function getDexterityModifier() {
        return $this->dexterity_modifier_race
            + $this->dexterity_modifier_equipment;
    }

    private function getDexterityModifierRace() {
        return $this->level * $this->race->dexterity_modifier;
    }

    private function getDexterityModifierEquipment() {
        return 0;
    }

    private function getConstitution() {
        return $this->constitution_base + $this->constitution_modifier;
    }

    private function getConstitutionModifier() {
        return $this->constitution_modifier_race
            + $this->constitution_modifier_equipment;
    }

    private function getConstitutionModifierRace() {
        return $this->level * $this->race->constitution_modifier;
    }

    private function getConstitutionModifierEquipment() {
        return 0;
    }

    private function getIntelligenceBase() {
        return 5 + $this->intelligence_base;
    }

    private function getIntelligenceModifier() {
        return $this->intelligence_modifier_race
            + $this->intelligence_modifier_equipment;
    }

    private function getIntelligenceModifierRace() {
        return $this->level * $this->race->intelligence_modifier;
    }

    private function getIntelligenceModifierEquipment() {
        return 0;
    }

    private function getWisdom() {
        return $this->wisdom_base + $this->wisdom_modifier;
    }

    private function getWisdomModifier() {
        return $this->wisdom_modifier_race
            + $this->wisdom_modifier_equipment;
    }

    private function getWisdomModifierRace() {
        return $this->level * $this->race->wisdom_modifier;
    }

    private function getWisdomModifierEquipment() {
        return 0;
    }

    private function getCharisma() {
        return $this->charisma_base + $this->charisma_modifier;
    }

    private function getCharismaModifier() {
        return $this->charisma_modifier_race
            + $this->charisma_modifier_equipment;
    }

    private function getCharismaModifierRace() {
        return $this->level * $this->race->charisma_modifier;
    }

    private function getCharismaModifierEquipment() {
        return 0;
    }

    private function getHitPoints() {
        return max(0, $this->hit_points_max - $this->wounds);
    }

    private function getHitPointsMax() {
        return $this->constitution * 4;
    }

}


function experience_to_level($experience) {
    return max(1, ((int) ($experience * 3 / 10) ** (1 / 3)) + 1);
}


function level_to_experience($level) {
    return max(0, round((10 * (($level - 1) ** 3)) / 3));
}


function create_character($character) {
    $id = db_create_id();
    db_query(
        '
            INSERT INTO characters (id, name, race, gender)
            VALUES (:id, :name, :race, :gender)
        ',
        [
            ':id' => $id,
            ':name' => $character['name'],
            ':race' => $character['race'],
            ':gender' => $character['gender']
        ]
    );
    return $id;
}


function find_character_by_id($id) {
    return Character::fromArray(db_query_first(
        'SELECT * FROM characters WHERE id = :id',
        [':id' => $id]
    ));
}


function find_characters_by_id($ids) {
    if (!$ids) return [];
    $placeholders = str_repeat('?, ', count($ids) - 1) . '?';
    $results = db_query_all(
        "SELECT * FROM characters WHERE id IN ($placeholders)",
        $ids
    );
    return Character::fromArrays($results);
}
