<?php

$location = location('gariki', new Location());

$location->name = 'Gariki';
$location->description = 'A dirty, old town in the middle of nowhere.';

$talk_to_freida = $location->talk('freida', 'Freida');
$talk_to_freida->condition = function () {
    return true;
};

$travel_to_the_abysmal_burrows = $location->travel(
    'the_abysmal_burrows', 'The Abysmal Burrows');

?>
