<?php

$location = location('the_abysmal_burrows', new Location());

$location->name = 'The Abysmal Burrows';
$location->description = 'A deep dungeon on the outskirts of Gariki.';

$travel_to_gariki = $location->travel('gariki', 'Gariki');

?>
