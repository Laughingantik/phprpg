<?php


class User {

    public $id = null;
    public $email = null;
    public $password = null;
    public $created_at = null;
    public $updated_at = null;
    public $cached_parties = null;

    static function fromArray($data) {
        if ($data == null) return null;
        $user = new User();
        if (isset($data['id'])) $user->id = $data['id'];
        $user->email = $data['email'];
        $user->password = $data['password'];
        if (isset($data['created_at'])) $user->created_at = $data['created_at'];
        if (isset($data['updated_at'])) $user->updated_at = $data['updated_at'];
        return $user;
    }

    static function fromArrays($data) {
        return array_map('User::fromArray', $data);
    }

    public function __get($name) {
        switch ($name) {
            case 'parties': return $this->getParties();
            default:
                throw new InvalidArgumentException("Invalid property: $name");
        }
    }

    private function getParties() {
        if (!$this->cached_parties)
            $this->cached_parties = find_parties_by_user_id($this->id);
        return $this->cached_parties;
    }

}


function validate_user($user) {
    return validate_user_credentials($user);
}


function validate_user_credentials($credentials) {
    $errors = [];
    if (!$credentials['email']) $errors[] = 'Email address is required.';
    if (!$credentials['password']) $errors[] = 'Password is required.';
    return $errors;
}


function create_user($user) {
    $id = db_create_id();
    db_query(
        '
            INSERT INTO users (id, email, password)
            VALUES (:id, :email, :password)
        ',
        [
            ':id' => $id,
            ':email' => $user['email'],
            ':password' => $user['password']
        ]
    );
    return $id;
}


function find_user_by_id($id) {
    return User::fromArray(db_query_first(
        'SELECT * FROM users WHERE id = :id',
        [':id' => $id]
    ));
}


function find_user_by_email($email) {
    return User::fromArray(db_query_first(
        'SELECT * FROM users WHERE email = :email',
        [':email' => $email]
    ));
}
