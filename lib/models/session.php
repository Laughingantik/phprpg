<?php


class Session {

    public $id = null;
    public $user_id = null;
    public $party_id = null;
    public $ip_address = null;
    public $user_agent = null;
    public $revoked = false;
    public $created_at = null;
    public $updated_at = null;
    public $expires_at = null;
    public $cached_user = null;
    public $cached_party = null;

    static function fromArray($data) {
        if ($data == null) return null;
        $session = new Session();
        if (isset($data['id'])) $session->id = $data['id'];
        $session->user_id = $data['user_id'];
        if (isset($data['party_id'])) $session->party_id = $data['party_id'];
        $session->ip_address = $data['ip_address'];
        $session->user_agent = $data['user_agent'];
        if (isset($data['revoked'])) $session->revoked = $data['revoked'];
        if (isset($data['created_at']))
            $session->created_at = $data['created_at'];
        if (isset($data['updated_at']))
            $session->updated_at = $data['updated_at'];
        $session->expires_at = $data['expires_at'];
        return $session;
    }

    static function fromArrays($data) {
        return array_map('Session::fromArray', $data);
    }

    public function __get($name) {
        switch ($name) {
            case 'user': return $this->getUser();
            case 'party': return $this->getParty();
            default:
                throw new InvalidArgumentException("Invalid property: $name");
        }
    }

    private function getUser() {
        if (!$this->cached_user and $this->user_id)
            $this->cached_user = find_user_by_id($this->user_id);
        return $this->cached_user;
    }

    private function getParty() {
        if (!$this->cached_party and $this->party_id)
            $this->cached_party = find_party_by_id($this->party_id);
        return $this->cached_party;
    }

}


function create_session($session) {
    $id = db_create_id();
    $expires_at = date('Y-m-d H:i:s', time() + SESSION_TIMEOUT);
    db_query(
        '
            INSERT INTO sessions (id, user_id, ip_address, user_agent,
                expires_at)
            VALUES (:id, :user_id, :ip_address, :user_agent, :expires_at)
        ',
        [
            ':id' => $id,
            ':user_id' => $session['user_id'],
            ':ip_address' => $session['ip_address'],
            ':user_agent' => $session['user_agent'],
            ':expires_at' => $expires_at
        ]
    );
    return $id;
}


function find_session_by_id($id) {
    return Session::fromArray(db_query_first(
        'SELECT * FROM sessions WHERE id = :id',
        [':id' => $id]
    ));
}


function revoke_session($session) {
    db_query(
        'UPDATE sessions SET revoked = TRUE WHERE id = :id',
        [':id' => $session->id]
    );
}


function refresh_session($session) {
    $expires_at = date('Y-m-d H:i:s', time() + SESSION_TIMEOUT);
    db_query(
        'UPDATE sessions SET expires_at = :expires_at WHERE id = :id',
        [':expires_at' => $expires_at, ':id' => $session->id]
    );
}


function session_expired($session) {
    return time() > strtotime($session->expires_at);
}


function set_session_party($session, $party) {
    set_session_party_id($session, $party->id);
}


function set_session_party_id($session, $party_id) {
    db_query(
        'UPDATE sessions SET party_id = :party_id WHERE id = :id',
        [':party_id' => $party_id, ':id' => $session->id]
    );
}
