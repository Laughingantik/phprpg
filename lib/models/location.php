<?php


class Location {

    public $key = null;
    public $name = null;
    public $description = null;
    private $interactions = [];

    public function attack($target, $target_name) {
        $interaction = new LocationAttackInteraction();
        $interaction->setTarget($target, $target_name);
        $this->interactions[] = $interaction;
        return $interaction;
    }

    public function quest($quest, $quest_name) {
        $interaction = new LocationQuestInteraction();
        $interaction->setTarget($quest, $quest_name);
        $this->interactions[] = $interaction;
        return $interaction;
    }

    public function talk($person, $person_name) {
        $interaction = new LocationTalkInteraction();
        $interaction->setTarget($person, $person_name);
        $this->interactions[] = $interaction;
        return $interaction;
    }

    public function travel($location, $location_name) {
        $interaction = new LocationTravelInteraction();
        $interaction->setTarget($location, $location_name);
        $this->interactions[] = $interaction;
        return $interaction;
    }

    public function use_object($item, $item_name, $target, $target_name) {
        $interaction = new LocationUseObjectInteraction();
        $interaction->setTarget($target, $target_name);
        $interaction->setObject($item, $item_name);
        $this->interactions[] = $interaction;
        return $interaction;
    }

    public function interact($action, $target = null, $object = null) {
        foreach ($interactions as $interaction) {
            $match = $interaction->action == $action
                and $interaction->target == $target
                and $interaction->object == $object;
            if ($match) {
                $interaction->interact();
            }
        }
    }

}


abstract class LocationInteraction {

    public $action = null;
    public $action_name = null;
    public $target = null;
    public $target_name = null;
    public $object = null;
    public $object_name = null;
    public $condition = null;

    public function setAction($action, $action_name) {
        $this->action = $action;
        $this->action_name = $action_name;
    }

    public function setTarget($target, $target_name) {
        $this->target = $target;
        $this->target_name = $target_name;
    }

    public function setObject($object, $object_name) {
        $this->object = $object;
        $this->object_name = $object_name;
    }

    abstract public function describe();

    abstract public function interact();

}


class LocationAttackInteraction extends LocationInteraction {

    public function __construct() {
        $this->setAction('attack', 'Attack');
    }

    public function describe() {
        return "Attack $this->target_name";
    }

    public function interact() {

    }

}


class LocationQuestInteraction extends LocationInteraction {

    public function __construct() {
        $this->setAction('quest', 'Quest');
    }

    public function describe() {
        $quest_complete = false;
        if ($quest_complete) return "Complete $this->target_name";
        return "Accept $this->target_name";
    }

    public function interact() {

    }

}


class LocationTalkInteraction extends LocationInteraction {

    public function __construct() {
        $this->setAction('talk', 'Talk');
    }

    public function describe() {
        return "Talk to $this->target_name";
    }

    public function interact() {

    }

}


class LocationTravelInteraction extends LocationInteraction {

    public function __construct() {
        $this->setAction('travel', 'Travel');
    }

    public function describe() {
        return "Travel to $this->target_name";
    }

    public function interact() {

    }

}


class LocationUseObjectInteraction extends LocationInteraction {

    public function __construct() {
        $this->setAction('use', 'Use');
    }

    public function describe() {
        if (!$this->target) return "Use $this->object_name";
        return "Use $this->object_name on $this->target_name";
    }

    public function interact() {

    }

}


function random_location() {
    return locations()[random_location_key()];
}


function random_location_key() {
    $keys = location_keys();
    return $keys[array_rand($keys)];
}


function location_keys() {
    return array_keys(locations());
}


function location_key($location) {
    foreach (locations() as $key => $value) {
        if ($location == $value) return $key;
    }
    return null;
}


function locations() {
    return location();
}


function location($key = null, $value = null) {
    static $locations = [];
    if ($key and $value) $locations[$key] = $value;
    if (!$key) return $locations;
    if (array_key_exists($key, $locations)) return $locations[$key];
    return null;
}
