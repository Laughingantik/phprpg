<?php

$race = character_race('elf', new CharacterRace());

$race->name = 'Elf';

$race->first_names = [
    'male' => [
        'Adran', 'Aelar', 'Aerdeth', 'Ahvain', 'Aramil', 'Arannis', 'Aust',
        'Azaki', 'Beiro', 'Berrian', 'Caeldrim', 'Carric', 'Dayereth', 'Dreali',
        'Efferil', 'Eiravel', 'Enialis', 'Erdan', 'Erevan', 'Fivin',
        'Galinndan', 'Gennal', 'Hadarai', 'Halimath', 'Heian', 'Himo',
        'Immeral', 'Ivellios', 'Korfel', 'Lamlis', 'Laucian', 'Lucan',
        'Mindartis', 'Naal', 'Nutae', 'Paelias', 'Peren', 'Quarion', 'Riardon',
        'Rolen', 'Soveliss', 'Suhnae', 'Thamior', 'Tharivol', 'Theren',
        'Theriatis', 'Thervan', 'Uthemar', 'Vanuath', 'Varis'],
    'female' => [
        'Adrie', 'Ahinar', 'Althaea', 'Anastrianna', 'Andraste', 'Antinua',
        'Arara', 'Baelitae', 'Bethrynna', 'Birel', 'Caelynn', 'Chaedi',
        'Claira', 'Dara', 'Drusilia', 'Elama', 'Enna', 'Faral', 'Felosial',
        'Hatae', 'Ielenia', 'Ilanis', 'Irann', 'Jarsali', 'Jelenneth',
        'Keyleth', 'Leshanna', 'Lia', 'Maiathah', 'Malquis', 'Meriele',
        'Mialee', 'Myathethil', 'Naivara', 'Quelenna', 'Quillathe', 'Ridaro',
        'Sariel', 'Shanairla', 'Shava', 'Silaqui', 'Sumnes', 'Theirastra',
        'Thiala', 'Tiaathque', 'Traulam', 'Vadania', 'Valanthe', 'Valna',
        'Xanaphia'],
    'nonbinary' => []
];

$race->last_names = ['Aloro', 'Amakiir', 'Amastacia', 'Ariessus',
    'Arnuanna', 'Berevan', 'Caerdonel', 'Caphaxath', 'Casilltenirra',
    'Cithreth', 'Dalanthan', 'Eathalena', 'Erenaeth', 'Ethanasath',
    'Fasharash', 'Firahel', 'Floshem', 'Galanodel', 'Goltorah',
    'Hanali', 'Holimion', 'Horineth', 'Iathrana', 'Ilphelkiir',
    'Iranapha', 'Koehlanna', 'Lathalas', 'Liadon', 'Meliamne',
    'Mellerelel', 'Mystralath', 'Naïlo', 'Netyoive', 'Ofandrus',
    'Ostoroth', 'Othronus', 'Qualanthri', 'Raethran', 'Rothenel',
    'Selevarun', 'Siannodel', 'Suithrasas', 'Sylvaranth',
    'Teinithra', 'Tiltathana', 'Wasanthi', 'Withrethin',
    'Xiloscient', 'Xistsrith', 'Yaeldrin'];

$race->dexterity_modifier = 2;
$race->intelligence_modifier = 2;
$race->wisdom_modifier = 2;

?>
