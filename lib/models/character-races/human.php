<?php

$race = character_race('human', new CharacterRace());

$race->name = 'Human';

$race->first_names = [
    'male' => [
        # celtic:
        'Airell', 'Airic', 'Alan', 'Anghus', 'Aodh', 'Bardon',
        'Bearacb', 'Bevyn', 'Boden', 'Bran', 'Brasil', 'Bredon',
        'Brian', 'Bricriu', 'Bryant', 'Cadman', 'Caradoc', 'Cedric',
        'Conalt', 'Conchobar', 'Condon', 'Darcy', 'Devin', 'Dillion',
        'Donaghy', 'Donall', 'Duer', 'Eghan', 'Ewyn', 'Ferghus',
        'Galvyn', 'Gildas', 'Guy', 'Harvey', 'Iden', 'Irven', 'Karney',
        'Kayne', 'Kelvyn', 'Kunsgnos', 'Leigh', 'Maccus', 'Moryn',
        'Neale', 'Owyn', 'Pryderi', 'Reaghan', 'Taliesin', 'Tiernay',
        'Turi',
        # english:
        'Adam', 'Adelard', 'Aldous', 'Anselm', 'Arnold', 'Bernard',
        'Bertram', 'Charles', 'Clerebold', 'Conrad', 'Diggory', 'Drogo',
        'Everard', 'Frederick', 'Geoffrey', 'Gerald', 'Gilbert',
        'Godfrey', 'Gunter', 'Guy', 'Henry', 'Heward', 'Hubert', 'Hugh',
        'Jocelyn', 'John', 'Lance', 'Manfred', 'Miles', 'Nicholas',
        'Norman', 'Odo', 'Percival', 'Peter', 'Ralf', 'Randal',
        'Raymond', 'Reynard', 'Richard', 'Robert', 'Roger', 'Roland',
        'Rolf', 'Simon', 'Theobald', 'Theodoric', 'Thomas', 'Timm',
        'William', 'Wymar',
        # norse:
        'Agni', 'Alaric', 'Anvindr', 'Arvid', 'Asger', 'Asmund',
        'Bjarte', 'Bjorg', 'Bjorn', 'Brandr', 'Brandt', 'Brynjar',
        'Calder', 'Colborn', 'Cuyler', 'Egil', 'Einar', 'Eric',
        'Erland', 'Fiske', 'Folkvar', 'Fritjof', 'Frode', 'Geir',
        'Halvar', 'Hemming', 'Hjalmar', 'Hjortr', 'Ingimarr', 'Ivar',
        'Knud', 'Leif', 'Liufr', 'Manning', 'Oddr', 'Olin', 'Ormr',
        'Ove', 'Rannulfr', 'Sigurd', 'Skari', 'Snorri', 'Sten',
        'Stigandr', 'Stigr', 'Sven', 'Trygve', 'Ulf', 'Vali', 'Vidar'],
    'female' => [
        # celtic:
        'Aife', 'Aina', 'Alane', 'Ardena', 'Arienh', 'Beatha', 'Birgit',
        'Briann', 'Caomh', 'Cara', 'Cinnia', 'Cordelia', 'Deheune',
        'Divone', 'Donia', 'Doreena', 'Elsha', 'Enid', 'Ethne',
        'Evelina', 'Fianna', 'Genevieve', 'Gilda', 'Gitta', 'Grania',
        'Gwyndolin', 'Idelisa', 'Isolde', 'Keelin', 'Kennocha',
        'Lavena', 'Lesley', 'Linnette', 'Lyonesse', 'Mabina', 'Marvina',
        'Mavis', 'Mirna', 'Morgan', 'Muriel', 'Nareena', 'Oriana',
        'Regan', 'Ronat', 'Rowena', 'Selma', 'Ula', 'Venetia', 'Wynne',
        'Yseult',
        # english:
        'Adelaide', 'Agatha', 'Agnes', 'Alice', 'Aline', 'Anne',
        'Avelina', 'Avice', 'Beatrice', 'Cecily', 'Egelina', 'Eleanor',
        'Elizabeth', 'Ella', 'Eloise', 'Elysande', 'Emeny', 'Emma',
        'Emmeline', 'Ermina', 'Eva', 'Galiena', 'Geva', 'Giselle',
        'Griselda', 'Hadwisa', 'Helen', 'Herleva', 'Hugolina', 'Ida',
        'Isabella', 'Jacoba', 'Jane', 'Joan', 'Juliana', 'Katherine',
        'Margery', 'Mary', 'Matilda', 'Maynild', 'Millicent', 'Oriel',
        'Rohesia', 'Rosalind', 'Rosamund', 'Sarah', 'Susannah', 'Sybil',
        'Williamina', 'Yvonne',
        # norse:
        'Alfhild', 'Arnbjorg', 'Ase', 'Aslog', 'Astrid', 'Auda',
        'Audhid', 'Bergljot', 'Birghild', 'Bodil', 'Brenna', 'Brynhild',
        'Dagmar', 'Eerika', 'Eira', 'Gudrun', 'Gunborg', 'Gunhild',
        'Gunvor', 'Helga', 'Hertha', 'Hilde', 'Hillevi', 'Ingrid',
        'Iona', 'Jorunn', 'Kari', 'Kenna', 'Magnhild', 'Nanna', 'Olga',
        'Ragna', 'Ragnhild', 'Ranveig', 'Runa', 'Saga', 'Sigfrid',
        'Signe', 'Sigrid', 'Sigrunn', 'Solveg', 'Svanhild', 'Thora',
        'Torborg', 'Torunn', 'Tove', 'Unn', 'Vigdis', 'Ylva', 'Yngvild'],
    'nonbinary' => []
];

$race->last_names = ['Abbington', 'Abordieu', 'Abril', 'Albania',
    'Albimbert', 'Albion', 'Albizia', 'Alinac', 'Amberflaw',
    'Amberflayer', 'Amberhide', 'Ambers', 'Ambrose', 'Andilet', 'Andre',
    'Ashbluff', 'Ashsorrow', 'Astaseul', 'Asteria', 'Autumnbow',
    'Barleyjumper', 'Bellevue', 'Belmont', 'Bertillon', 'Bizeveron',
    'Blackmark', 'Bluebleeder', 'Bobellon', 'Boneflare', 'Bonnie',
    'Bougaimoux', 'Bougaitelet', 'Boulderward', 'Brichallard',
    'Brichazac', 'Brightdoom', 'Broffet', 'Brownie', 'Béchalot',
    'Castedras', 'Castemont', 'Chabaffet', 'Chabares', 'Chamillet',
    'Chanalet', 'Chanassard', 'Chauveron', 'Chauvet',
    'Cinderbreaker', 'Cinderhell', 'Clanwillow', 'Clanwing',
    'Clawarm', 'Clawroot', 'Clearpunch', 'Cliffdane', 'Cliffless',
    'Coldblight', 'Coldcloud', 'Coldsprinter', 'Commonseeker',
    'Covenbreath', 'Cragore', 'Credieu', 'Cremeur', 'Crestbreeze',
    'Cretillon', 'Crowstrike', 'Crystalbone', 'Dawnless', 'Deep',
    'Deepwing', 'Deepwoods', 'Dewbringer', 'Distantfury',
    'Distantwind', 'Dragoncutter', 'Dumieres', 'Duras', 'Duskbloom',
    'Duskstalker', 'Dustseeker', 'Echethier', 'Elfbreath',
    'Elffire', 'Elfscribe', 'Elfwind', 'Embershadow', 'Fallenorb',
    'Fang', 'Featherbrew', 'Featherdreamer', 'Featherswallow',
    'Fern', 'Finecrusher', 'Flame', 'Flameshaper', 'Flatstrider',
    'Flatwatcher', 'Fog', 'Forebluff', 'Foreswift', 'Forge',
    'Fourspire', 'Fourswallow', 'Frozenreaper', 'Frozenscribe',
    'Gaignory', 'Gaillot', 'Gaimbert', 'Gairil', 'Ginemoux',
    'Ginerisey', 'Gloryrock', 'Gloryweaver', 'Gorelight',
    'Grandsplitter', 'Grasshammer', 'Hallowedchaser',
    'Hallowedsorrow', 'Hallowshadow', 'Hallowswift', 'Hardarm',
    'Hardshout', 'Havenash', 'Havendoom', 'Havenglow', 'Hazekeep',
    'Hazerider', 'Hellbough', 'Honorhorn', 'Humblebringer',
    'Humblecut', 'Humblereaper', 'Hydrabreath', 'Icehand',
    'Ironcut', 'Irongrip', 'Jouvempes', 'Jouvessac', 'Keenfollower',
    'Keenstone', 'Lamadras', 'Lamagnon', 'Lamanie', 'Lamogre',
    'Larmagnory', 'Larmalart', 'Larmalleve', 'Larmanton',
    'Laughingroar', 'Laughingsnout', 'Lauregnory', 'Leafdream',
    'Leafslayer', 'Leafwater', 'Lightscream', 'Lignichanteau',
    'Lignignon', 'Limochanteau', 'Lomadieu', 'Lonerider',
    'Longshard', 'Machenet', 'Macherac', 'Macherral', 'Maignes',
    'Maignes', 'Mailon', 'Marblemaw', 'Marbletail', 'Marblewhisper',
    'Marblewing', 'Marshrider', 'Massouchanteau', 'Massoulleve',
    'Massoumbert', 'Masterfang', 'Masterjumper', 'Meadowbrace',
    'Mildbreath', 'Mildstrike', 'Mirthcleaver', 'Mirthhorn',
    'Mirthmantle', 'Mistbinder', 'Mistblood', 'Misteyes',
    'Moltenore', 'Moltentide', 'Monsterbelly', 'Montalli',
    'Montanne', 'Montarac', 'Mountainbane', 'Mourningsnow',
    'Mournmoon', 'Mournrock', 'Neredras', 'Nicklegrain',
    'Nicklewhisk', 'Nightwind', 'Nobledane', 'Nobledrifter',
    'Noblestrike', 'Oatcrag', 'Oatshine', 'Oattaker', 'Ocean',
    'Oceancut', 'Oceanseeker', 'Orbarrow', 'Orbstrike', 'Paleforce',
    'Peacescream', 'Pellelles', 'Pellerelli', 'Plaingrove',
    'Pridesong', 'Pridewood', 'Proudchaser', 'Proudfollower',
    'Proudswift', 'Rainward', 'Rambumoux', 'Rapidclaw', 'Rapidroot',
    'Redshadow', 'Regalhelm', 'Regalshade', 'Richshout', 'Rochegne',
    'Rochelieu', 'Rocheveron', 'Ronchegnac', 'Ronchelieu',
    'Ronchessac', 'Roquenet', 'Roqueze', 'Rosedreamer', 'Roserun',
    'Rosewhisper', 'Roughdust', 'Roughforest', 'Roughwhirl',
    'Rumbleash', 'Runebraid', 'Sacredmore', 'Sacredpelt',
    'Sagepunch', 'Sagesun', 'Sarramond', 'Saurmaw', 'Serpentbrook',
    'Shadegrove', 'Shadowflaw', 'Sharpblade', 'Sharpdoom',
    'Shieldgem', 'Shieldtrap', 'Silentbrace', 'Silverweaver',
    'Skyfire', 'Skyhunter', 'Skyshade', 'Skysnow', 'Slateflayer',
    'Smartlash', 'Smartreaper', 'Snowscar', 'Softgloom', 'Solidcut',
    'Spiritglade', 'Spiritscribe', 'Springbender', 'Springspell',
    'Steelpike', 'Steelrunner', 'Sternguard', 'Sternshine',
    'Stillblade', 'Stonebender', 'Stormorb', 'Stoutspirit',
    'Strongblaze', 'Sufelon', 'Suteuil', 'Suva', 'Swiftbrew',
    'Tarrencloud', 'Tarrenseeker', 'Terramaul', 'Terrarock',
    'Terraspear', 'Terrawater', 'Thundermourn', 'Titantoe',
    'Treelash', 'Treeshaper', 'Truthbelly', 'Tusksnarl', 'Twoaxe',
    'Vassezac', 'Vegne', 'Vernifelon', 'Vernillard', 'Verninne',
    'Vernize', 'Virac', 'Voidbend', 'Voidlash', 'Voidreaper',
    'Warbelly', 'Warbreaker', 'Warmane', 'Warmight', 'Wheatbrow',
    'Wheatglow', 'Whispercrest', 'Whitemoon', 'Whitwatcher',
    'Wildforce', 'Wildwhirl', 'Winddane', 'Winterwound', 'Wisekeep',
    'Wisekiller', 'Wolfgrain', 'Woodflower', 'Woodlight',
    'Wyvernseeker', 'Youngvigor'];

$race->strength_modifier = 1;
$race->dexterity_modifier = 1;
$race->constitution_modifier = 1;
$race->intelligence_modifier = 1;
$race->wisdom_modifier = 1;
$race->charisma_modifier = 1;

?>
