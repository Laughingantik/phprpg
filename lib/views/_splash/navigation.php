<?php if (user_logged_in()) { ?>
<ul class="nav nav-pills justify-content-center">
  <li class="nav-item">
    <a class="nav-link" href="parties.php">Parties</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="logout.php">Log out</a>
  </li>
</ul>
<?php } else { ?>
<ul class="nav nav-pills justify-content-center">
  <li class="nav-item">
    <a class="nav-link" href="index.php">Home</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="login.php">Log in</a>
  </li>
  <li class="nav-item">
    <a class="nav-link" href="register.php">Register</a>
  </li>
</ul>
<?php } ?>
