<?php require_once VIEW_DIR . '_common/form-errors.php'; ?>
<script>
    $(document).ready(function () {
        $('#randomNameButton').click(function () {
            const url = 'api/character-names.php'
            const params = {
                race: $("select[name='race']").val(),
                gender: $("select[name='gender']").val()
            }
            axios.get(url, { params: params }).then(function (response) {
                $("input[name='name']").val(response.data.names[0])
            })
        })
    })
</script>
<form action="parties.php?action=create" method="post">
    <div id="section1">
        <p>Bad news, my friend. You died.</p>
        <p>But not to worry!</p>
        <p>
            I've been working on a new project, and I think your soul would be
            perfect for it.
        </p>
        <p>I'll even let you choose how you want to be reincarnated!</p>
        <p>Let me just collect some information from you first.</p>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#section2" aria-expanded="false"
                aria-controls="section2">Next</button>
        </p>
    </div>
    <div id="section2" class="collapse">
        <p>The world you'll be born into is inhabited by several races.</p>
        <p>Which race would you like to be?</p>
        <p>
            <div class="form-group">
                <select name="race" class="form-control">
                    <option value="">(Race)</option>
                    <option value="dwarf">Dwarf</option>
                    <option value="elf">Elf</option>
                    <option value="human">Human</option>
                </select>
            </div>
        </p>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#section3" aria-expanded="false"
                aria-controls="section3">Next</button>
        </p>
    </div>
    <div id="section3" class="collapse">
        <p>Which gender would you like be?</p>
        <p>You don't have to pick the one you had before you died, you know.</p>
        <p>
            <div class="form-group">
                <select name="gender" class="form-control">
                    <option value="">(Gender)</option>
                    <option value="male">Male</option>
                    <option value="female">Female</option>
                    <option value="nonbinary">Non-binary</option>
                </select>
            </div>
        </p>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#section4" aria-expanded="false"
                aria-controls="section4">Next</button>
        </p>
    </div>
    <div id="section4" class="collapse">
        <p>And, what would you like to be called?</p>
        <p>
            <div class="input-group">
                <input type="text" name="name" class="form-control"
                    placeholder="Name" aria-label="Character's Name">
                <div class="input-group-append">
                    <button class="btn btn-secondary" type="button"
                        id="randomNameButton">Random</button>
                </div>
            </div>
        </p>
        <p>
            <button class="btn btn-primary" type="button" data-toggle="collapse"
                data-target="#section5" aria-expanded="false"
                aria-controls="section5">Next</button>
        </p>
    </div>
    <div id="section5" class="collapse">
        <p>You will become the leader of a party of adventurers.</p>
        <p>
            Well... for now, you'll be on your own, but get yourself out there
            and find some companions.
        </p>
        <p>Good luck! Toodles!</p>
        <p>
            <button type="submit" class="btn btn-success">Create Party</button>
        </p>
    </div>
</form>
