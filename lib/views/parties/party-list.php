<?php if (!current_user()->parties) { ?>
<p>You have no parties.</p>
<?php } ?>

<?php foreach (current_user()->parties as $party) { ?>
<p>
    Party: <?php echo $party->name; ?>
    <br>
    Leader: <?php echo $party->leader->name; ?>
    (<?php echo "{$party->leader->race->name} {$party->leader->gender->name}"; ?>)
    <br>
    <a href="parties.php?action=select&id=<?php echo $party->id;?>">Select</a>
</p>
<?php } ?>

<?php if (count(current_user()->parties) < 6) { ?>
<a href="parties.php?action=create">Create a new party</a>
<?php } ?>
