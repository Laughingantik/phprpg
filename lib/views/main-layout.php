<html>
    <head>
        <title>phprpg</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery-3.6.0.slim.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/axios.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <?php require_once VIEW_DIR . '_main/logo.php'; ?>
                    <br>
                    <?php require_once VIEW_DIR . '_main/navigation.php'; ?>
                </div>
                <div class="col-md-6">
                    <?php require_once VIEW_DIR . '_main/location.php'; ?>
                    <br>
                    <?php require_once VIEW_DIR . '_main/content.php'; ?>
                </div>
                <div class="col-md-3">
                    <?php require_once VIEW_DIR . '_main/party.php'; ?>
                </div>
            </div>
        </div>
    </body>
</html>
