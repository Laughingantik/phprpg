<form method="post" action="register.php">
    <div class="form-group">
        <label for="email">Email Address:</label>
        <input type="email" class="form-control" id="email" name="email"
            value="<?php echo $input['email']; ?>">
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password"
            name="password">
    </div>
    <div class="form-group">
        <label for="password_confirmation">Confirm Password:</label>
        <input type="password" class="form-control" id="password_confirmation"
            name="password_confirmation">
    </div>
  <button type="submit" class="btn btn-primary">Register</button>
</form>
