<?php
# remembered field values
$email = '';
if (isset($input)) {
    if (isset($input['email'])) $email = $input['email'];
}

require_once VIEW_DIR . '_common/form-errors.php';
?>
<form method="post" action="login.php">
    <div class="form-group">
        <label for="email">Email Address:</label>
        <input type="email" class="form-control" id="email" name="email"
            value="<?php echo $email ?>">
    </div>
    <div class="form-group">
        <label for="password">Password:</label>
        <input type="password" class="form-control" id="password"
            name="password">
    </div>
    <button type="submit" class="btn btn-primary">Log in</button>
</form>
