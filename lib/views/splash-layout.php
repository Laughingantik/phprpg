<html>
    <head>
        <title>phprpg</title>
        <link rel="stylesheet" href="css/bootstrap.min.css">
        <link rel="stylesheet" href="css/style.css">
        <script src="js/jquery-3.6.0.slim.min.js"></script>
        <script src="js/bootstrap.bundle.min.js"></script>
        <script src="js/axios.min.js"></script>
    </head>
    <body>
        <div class="container">
            <div class="row">
                <div class="col text-center">
                    [ splash logo here ]
                </div>
            </div>
            <div class="row">
                <div class="col">
                    <?php require_once VIEW_DIR . '_splash/navigation.php'; ?>
                </div>
            </div>
            <div class="row">
                <div class="col text-center">
                    <?php require_once VIEW_DIR . $content_view; ?>
                </div>
            </div>
        </div>
    </body>
</html>
