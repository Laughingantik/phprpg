<div class="card">
    <div class="card-body">
        <p class="card-text">
            Party: <?php echo current_party()->name; ?>
            <br>
            GP <small><?php echo number_format(current_party()->gold_coins); ?>
                </small>
            | SP <small><?php echo current_party()->silver_coins; ?></small>
            | CP <small><?php echo current_party()->copper_coins; ?></small>
        </p>
    </div>
</div>

<br>

<?php foreach (current_party_members() as $member) { ?>
<div class="card">
    <div class="card-body">
        <p class="card-text">
            <?php echo $member->name; ?>
            <br>
            (<?php echo "{$member->race->name} {$member->gender->name}"; ?>)
            <br>
            LVL <small><?php echo $member->level; ?></small>
            | EXP <small><?php echo number_format($member->experience); ?>
                / <?php echo number_format($member->experience_next); ?></small>
            <br>
            HP <small><?php echo number_format($member->hit_points); ?>
                / <?php echo number_format($member->hit_points_max); ?></small>
        </p>
    </div>
</div>
<br>
<?php } ?>

<?php for ($i = 0; $i < 6 - count(current_party_members()); $i++) {  ?>
<div class="card">
    <div class="card-body">
        <p class="card-text">(Empty Slot)</p>
    </div>
</div>
<br>
<?php } ?>
