
START TRANSACTION;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

DELETE FROM sessions;
DELETE FROM parties;
DELETE FROM users;
DELETE FROM characters;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

INSERT INTO users (id, email, password)
VALUES
    (99381213315203072, 'john@example.com',
        '$2y$10$GoVZUt6IxwANqgpEgCep0O5EGMRwCBpZrDksFTYtbXSd6AnI8ZBQe'),
    (99381213315203073, 'jane@example.com',
        '$2y$10$GoVZUt6IxwANqgpEgCep0O5EGMRwCBpZrDksFTYtbXSd6AnI8ZBQe'),
    (99381213315203074, 'jack@example.com',
        '$2y$10$GoVZUt6IxwANqgpEgCep0O5EGMRwCBpZrDksFTYtbXSd6AnI8ZBQe'),
    (99381213315203075, 'jill@example.com',
        '$2y$10$GoVZUt6IxwANqgpEgCep0O5EGMRwCBpZrDksFTYtbXSd6AnI8ZBQe');

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

INSERT INTO characters (id, name, race, gender)
VALUES
    (99381213315203151, 'Malquis Raethran', 'elf', 'female'),
    (99381213315203155, 'Orsik Daerdahk', 'dwarf', 'male');

UPDATE characters SET experience_total = 9000 WHERE id = 99381213315203151;
UPDATE characters SET experience_total = 7420 WHERE id = 99381213315203155;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

INSERT INTO parties (id, user_id, member_1_id, member_2_id, member_3_id,
    member_4_id, member_5_id, member_6_id)
VALUES
    (99381213315203153, 99381213315203072, 99381213315203151,
        99381213315203155, NULL, NULL, NULL, NULL);

UPDATE parties
SET gold_coins = 1234, silver_coins = 56, copper_coins = 78
WHERE id = 99381213315203153;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

COMMIT;
