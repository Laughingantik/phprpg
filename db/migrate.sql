
CREATE TABLE IF NOT EXISTS migrations (
    version INT NOT NULL,
    executed_at DATETIME DEFAULT NOW(),
    PRIMARY KEY (version)
);

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

DROP PROCEDURE IF EXISTS migration_complete;

DELIMITER //
CREATE PROCEDURE migration_complete (IN version INT)
BEGIN

    INSERT INTO migrations (`version`) VALUES (version);

END //
DELIMITER ;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

DROP FUNCTION IF EXISTS migration_executed;

DELIMITER //
CREATE FUNCTION migration_executed (version INT)
RETURNS INT
BEGIN

    DECLARE migration_count INT;

    SELECT COUNT(*)
    INTO migration_count
    FROM migrations
    WHERE migrations.version = version;

    RETURN migration_count > 0;

END //
DELIMITER ;

-- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- -- --

DROP PROCEDURE IF EXISTS migrate;

DELIMITER //
CREATE PROCEDURE migrate()
BEGIN

    START TRANSACTION;

    IF NOT (SELECT migration_executed(2021072701)) THEN
        CREATE TABLE users (
            id BIGINT UNSIGNED NOT NULL,
            email VARCHAR(255) UNIQUE NOT NULL,
            password VARCHAR(255) NOT NULL,
            created_at DATETIME DEFAULT NOW(),
            updated_at DATETIME DEFAULT NOW() ON UPDATE NOW(),
            PRIMARY KEY (id)
        );
        CALL migration_complete(2021072701);
    END IF;

    IF NOT (SELECT migration_executed(2021080901)) THEN
        CREATE TABLE sessions (
            id BIGINT UNSIGNED NOT NULL,
            user_id BIGINT UNSIGNED NOT NULL,
            ip_address VARCHAR(64) NOT NULL,
            user_agent TEXT NOT NULL,
            revoked BOOLEAN NOT NULL DEFAULT FALSE,
            created_at DATETIME DEFAULT NOW(),
            updated_at DATETIME DEFAULT NOW() ON UPDATE NOW(),
            expires_at DATETIME NOT NULL,
            PRIMARY KEY (id),
            FOREIGN KEY (user_id) REFERENCES users (id)
        );
        CALL migration_complete(2021080901);
    END IF;

    IF NOT (SELECT migration_executed(2021081101)) THEN
        CREATE TABLE characters (
            id BIGINT UNSIGNED NOT NULL,
            name VARCHAR(64) DEFAULT NULL,
            race VARCHAR(64) NOT NULL,
            gender VARCHAR(16) NOT NULL,
            experience_total INT DEFAULT 0,
            strength_base INT DEFAULT 5,
            dexterity_base INT DEFAULT 5,
            constitution_base INT DEFAULT 5,
            intelligence_base INT DEFAULT 5,
            wisdom_base INT DEFAULT 5,
            charisma_base INT DEFAULT 5,
            wounds INT DEFAULT 0,
            created_at DATETIME DEFAULT NOW(),
            updated_at DATETIME DEFAULT NOW() ON UPDATE NOW(),
            PRIMARY KEY (id)
        );
        CREATE TABLE parties (
            id BIGINT UNSIGNED NOT NULL,
            user_id BIGINT UNSIGNED NOT NULL,
            member_1_id BIGINT UNSIGNED NOT NULL,
            member_2_id BIGINT UNSIGNED DEFAULT NULL,
            member_3_id BIGINT UNSIGNED DEFAULT NULL,
            member_4_id BIGINT UNSIGNED DEFAULT NULL,
            member_5_id BIGINT UNSIGNED DEFAULT NULL,
            member_6_id BIGINT UNSIGNED DEFAULT NULL,
            name VARCHAR(64) DEFAULT "(Unknown)",
            gold_coins INT DEFAULT 0,
            silver_coins INT DEFAULT 0,
            copper_coins INT DEFAULT 0,
            created_at DATETIME DEFAULT NOW(),
            updated_at DATETIME DEFAULT NOW() ON UPDATE NOW(),
            PRIMARY KEY (id),
            FOREIGN KEY (user_id) REFERENCES users (id),
            FOREIGN KEY (member_1_id) REFERENCES characters (id),
            FOREIGN KEY (member_2_id) REFERENCES characters (id),
            FOREIGN KEY (member_3_id) REFERENCES characters (id),
            FOREIGN KEY (member_4_id) REFERENCES characters (id),
            FOREIGN KEY (member_5_id) REFERENCES characters (id),
            FOREIGN KEY (member_6_id) REFERENCES characters (id)
        );
        ALTER TABLE sessions ADD party_id BIGINT UNSIGNED DEFAULT NULL;
        ALTER TABLE sessions ADD FOREIGN KEY (party_id) REFERENCES parties (id);
        CALL migration_complete(2021081101);
    END IF;

    COMMIT;

END //
DELIMITER ;

CALL migrate;
